# Upload File Post API

The /upload/file POST API is used for uploading files from the configurator onto transit, which is then dumped into S3 for later use. The valid file types are .png (images) and .ttf (fonts). For recieving files from the UI, we use multer.

Multer is a node.js middleware for handling multipart/form-data , which is primarily used for uploading files. It is written on top of busboy for maximum efficiency. Multer will not process any form which is not multipart ( multipart/form-data ).

Multer assigns the directory in which the file uploaded is stored in. We validate the file payload first, then its assigned a s3Uri path in which it is uploaded to. For any S3 related actions we use [[release scripts]].
