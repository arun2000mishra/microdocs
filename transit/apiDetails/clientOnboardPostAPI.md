# Client Onboard Post API

The /client/onboard POST API is used to carry out the [[clientOnboardWorkflow]].
The payload of consists of the following components:

1. clientID
2. merchantID
3. integrationStatus
4. theme
   The payload that this API receives is first parsed through a parser written in Rescript , to ensure type safety. after this the clientID is validated using getClientData() which reads from Mongo if the clientID exists or not, and if it does then the API throws the error. After this the we use the function fetchThemeAssets() which takes the theme as a parameter and then returns all the assets that are required as mentioned in [[clientOnboardWorkflow]]. These assets are then commited to bitbucket [[commitAssets]] and later uploaded to their respective s3 url paths [[uploadBetaAssets]]. After all this is over, the clientID is then added to the MongoDB using addNewClient() function
