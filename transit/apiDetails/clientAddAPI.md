# /client/add API

This API is used to add a client to a particular merchant. The payload that is passed into it has the following key information.

1. clientId,
2. merchantId,
3. staggerAccess,
4. releaseManagers,
5. integrationStatus,
6. minApprovals,
7. services,
8. apiVersion

The paylaod that is passed is first parserd to check whether the type and the content is valid or not. If the user is not an admin, and has added releaseManagers , then that is invalid as only admins are allowed to add releaseManagers.v
The merchantID validity is then checked to see if the merchant still exists in the DB. For getting this information externally, a get call can be made to /merchant GET API [[merchantGetAPI]]. The client is then added to the DB corresponding to the merchant in MongoDB.

## The DB has a schema level check to ensure that duplicate clientID for a particular merchantID.

## The functionality of this API is also carried out by Client Onboard API which is used ot carry out Client Onboard Workflow [[clientOnboardWorkflow]]
