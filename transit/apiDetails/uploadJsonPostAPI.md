# Upload Json Post API

/upload/json Post API is used to upload content into a file in S3. This is different from [[uploadFilePostAPI]] in the way that, here we are uploading a JSON instead of a file. There is a payload validation done, which checks if the file being uploaded is icons.json, strings.json, metaJson.json, qrData.json or dashboardMetaJson.json, after which the function "s3uriMapper" assigns the upload s3 url. The content is then uploaded to S3 using [[release scripts]] and the uploaded file is made public by which the json can be accessed. This is used only to upload assets.
