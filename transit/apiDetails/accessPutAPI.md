# Access Put API

## Redacted

The functions are automatically called inside [[clientOnboardAPI]] and access roles are assigned, this API is no longer being used.

This API is used to assign [[accessRoles]] to a new clientID.
