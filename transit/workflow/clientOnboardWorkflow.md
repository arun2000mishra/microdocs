# Client Onboard Workflow.

On Hyoperconfiguration, the relationship between a merchant and client is 1:n. That means, for every merchantID that we have there could be n number of clientIDs. When new clientID is added for a particular merchant there are 4 operations that take place, for the client to have their setup ready. Before, these steps were carried out in the frontend by calling 3 different APIs, but now they have been consolidation all these functionalities into a single API [[clientOnboardAPI]].

The steps that take place are as follows:

1. The three JS files which contain the assets (configuration.js, icons,js , strings.js) are downloaded from their respective links and then these assets are committed to bitbucket, with respect to the clientID, after which a remoteAssetSync function is called which uploads these assets to their respective platform(OS) and s3Uri [[updateConfigAPI]]

2. The access put API [[accessPutAPI]] is called , which gets the access.json file from S3 and updates it with the new user for the current client.

3. The client is then added to the MongoDB corresponding to the merchantID. [[clientAddAPI]]
