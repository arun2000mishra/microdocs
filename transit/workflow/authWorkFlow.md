# Authentication Workflow

Authentication APIs are also present in transit which is responsible for generating and validating token. During generation of token, the token contains information about the user, such as id, email address, googleId, name, createdAt and accessRoles. During generating the token, the API reads from the MongoDB, the information of the user and then this information is part of the generated token. During the token validating process all of this information is stored in the request.user object.
