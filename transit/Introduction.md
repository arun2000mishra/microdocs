# Transit

## Brief

Transit is isolated from Titan to carry out various tasks for Configurator. It works as a connection between Configurator frontend and S3. There are 3 main files which define the layout of the payment page- > Config, Strings and Icons. Strings file stores the different strings that are displayed in the payment page in the form of key value pairs. The icons file does the same for all the different icons being displayed. The config file is the more detailed file which contains all the information pertaining to the UI of the payment page. All of these are stored in S3.

We want to be able to download, and upload all three of these JS files to S3 (configuration, strings and icons.) Handled by [[updateConfigAPI]].
